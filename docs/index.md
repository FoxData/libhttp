# libhttp Documentation
libhttp documentation and usage
## Creating an object
```text
  -- Importing the library
  local libhttp = include "libhttp"
  -- Creating the object and assigning a URL
  local Connection = libhttp.HttpConnection:new()
  Connection.url = "https://example.com/"
```

## Objects
### `HttpConnection`
HTTP Connection object on which functions operate in
### `HttpRequest`
HTTP Request results that synchronous functions return.

## HttpConnection Properties
### `string HttpConnection.url`
URL that the object points to
### `table HttpConnection.parameters`
Table of unencoded parameters
### `table HttpConnection.headers`
Table of headers that is passed to requests

## HttpConnection Functions
### `HttpConnection:addParameter(string name, string value)` -> `boolean ok`
Add a parameter `name=value` to an HttpConnection. Returns true or false depending if the action was successful.
### `HttpConnection:removeParameter(string name)` -> `boolean ok`
Removes a parameter `name` from an HttpConnection. Returns true or false depending if the action was successful.
### `HttpConnection:encodeParameters()` -> `string encodedUrl`
Encodes parameters and appends them into an url.  
Example:
```text
  Connection.url = "https://example.com/api/"
  Connection:addParameter("action", "test")
  print(Connection:encodeParameters()) -- https://example.com/api/?action=test&
```
### `HttpConnection:addHeader(string name, string value)` -> `boolean ok`
Adds a header `name` with value `value`. Returns true or false depending if the action was successful.
### `HttpConnection:removeHeader(string name)` -> `boolean ok`
Removes a header `name`. Returns true or false depending if the action was successful.
### `HttpConnection:getAsync()` -> `nil`
Makes an asynchronous GET request with the current parameters and headers. Queues an event `http_success` or `http_failure` with the necessary data.
### `HttpConnection:postAsync()` -> `nil`
Makes an asynchronous POST request with the current parameters and headers. Queues an event `http_success` or `http_failure` with the necessary data.
### `HttpConnection:putAsync()` -> `nil`
Makes an asynchronous PUT request with the current parameters and headers. Queues an event `http_success` or `http_failure` with the necessary data.
### `HttpConnection:deleteAsync()` -> `nil`
Makes an asynchronous DELETE request with the current parameters and headers. Queues an event `http_success` or `http_failure` with the necessary data.
### `HttpConnection:get()` -> `HttpRequest result`
Makes a synchronous GET request with the current parameters and headers. Returns an HttpRequest object.
### `HttpConnection:post()` -> `HttpRequest result`
Makes a synchronous POST request with the current parameters and headers. Returns an HttpRequest object.
### `HttpConnection:put()` -> `HttpRequest result`
Makes a synchronous PUT request with the current parameters and headers. Returns an HttpRequest object.
### `HttpConnection:delete()` -> `HttpRequest result`
Makes a synchronous DELETE request with the current parameters and headers. Returns an HttpRequest object.

## HttpRequest Properties
### `string HttpRequest.content`
Contains the contents of the request.
### `number HttpRequest.responseCode`
Contains the HTTP response code.

## HttpRequest Functions
### `HttpRequest.readLines()` -> `Iterator lineReader`
Iterates the lines of the content of the request.
