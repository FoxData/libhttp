--[[
	libhttp | HTTP API Wrapper
	        | Pantheon Project (galaxiorg/pantheon)

	Filename
	  main.lua
	Author
	  daelvn (https://gitlab.com/daelvn)
	Version
		libhttp-0.1
	License
		MIT License (LICENSE.md)
  Platforms
    Pantheon
    Pantheon/Ckit
    CraftOS
    CraftOS-based
	Description
		libhttp is a wrapper around the native HTTP API that supports GET, POST, PUT
		and DELETE (only the two first are native) with an easy way of handling
		headers and parameters.
]]--

--- Create exportable ---
local libhttp = {}

--- Create OOP base ---
local HttpConnection = {}
function HttpConnection:new(obj)
  -- Create object
  obj = obj or {}
  if not obj.url then
    obj.url = ""
  elseif not obj.parameters then
    obj.parameters = {}
  elseif not obj.headers then
    obj.headers = {}
  end
  -- Inherit
  setmetatable(o, self)
  self.__index = self
  -- Return
  return obj
end

--- Functions ---
-- Add a parameter
function HttpConnection:addParameter(name, value)
  if (not name) or (not value) then return false end
  self.parameters[tostring(name)] = textutils.urlEncode(tostring(value))
  return true
end

-- Remove a parameter
function HttpConnection:removeParameter(name)
	if (not name) then return false end
	self.parameters[tostring(name)] = nil
	return true
end

-- Encode parameters for GET request
function HttpConnection:encodeParameters()
	local append = "?"
	for name, value in pairs(self.parameters) do
		append = append .. name .. "=" .. value .. "&"
	end
	return append
end

-- Add header
function HttpConnection:addHeader(name, value)
	if (not name) or (not value) then return false end
	self.headers[tostring(name)] = textutils.urlEncode(tostring(value))
	return true
end

-- Remove header
function HttpConnection:removeHeader(name)
	if (not name) then return false end
	self.headers[tostring(name)] = nil
	return true
end

-- Send GET request (async)
function HttpConnection:getAsync()
	http.request(self.url .. self:encodeParameters(), {}, self.headers)
end

-- Send POST request (async)
function HttpConnection:postAsync()
	http.request(self.url, self.parameters, self.headers)
end

-- Send PUT request (async)
function HttpConnection:putAsync()
  self:addHeader("X-HTTP-Method-Override", "PUT")
	http.request(self.url, self.parameters, self.headers)
end

-- Send DELETE request (async)
function HttpConnection:deleteAsync()
  self:addHeader("X-HTTP-Method-Override", "PUT")
	http.request(self.url, self.parameters, self.headers)
end

-- Send GET request (sync)
function HttpConnection:get()
	local NativeHttpRequest = http.get(self.url .. self:encodeParameters(), self.headers)
  local HttpRequest = {
    content = NativeHttpRequest.readAll(),
    responseCode = NativeHttpRequest.getResponseCode()
  }
  HttpRequest.readLines = function()
    local i = 0
    local ls = {}
    for l in HttpRequest.content:gmatch("(.+)\r?\n?") do
      table.insert(ls,l)
    end
    return function()
      i = i + 1
      return ls[i]
    end
  end
  NativeHttpRequest.close()
  return HttpRequest
end

-- Send POST request (sync)
function HttpConnection:post()
	local NativeHttpRequest = http.post(self.url, self.parameters, self.headers)
  local HttpRequest = {
    content = NativeHttpRequest.readAll(),
    responseCode = NativeHttpRequest.getResponseCode()
  }
  HttpRequest.readLines = function()
    local i = 0
    local ls = {}
    for l in HttpRequest.content:gmatch("(.+)\r?\n?") do
      table.insert(ls,l)
    end
    return function()
      i = i + 1
      return ls[i]
    end
  end
  NativeHttpRequest.close()
  return HttpRequest
end

-- Send PUT request (sync)
function HttpConnection:put()
	self:addHeader("X-HTTP-Method-Override", "PUT")
	local NativeHttpRequest = http.post(self.url, self.parameters, self.headers)
  local HttpRequest = {
    content = NativeHttpRequest.readAll(),
    responseCode = NativeHttpRequest.getResponseCode()
  }
  HttpRequest.readLines = function()
    local i = 0
    local ls = {}
    for l in HttpRequest.content:gmatch("(.+)\r?\n?") do
      table.insert(ls,l)
    end
    return function()
      i = i + 1
      return ls[i]
    end
  end
  NativeHttpRequest.close()
  return HttpRequest
end

-- Send DELETE request (sync)
function HttpConnection:delete()
	self:addHeader("X-HTTP-Method-Override", "DELETE")
	local NativeHttpRequest = http.post(self.url, self.parameters, self.headers)
  local HttpRequest = {
    content = NativeHttpRequest.readAll(),
    responseCode = NativeHttpRequest.getResponseCode()
  }
  HttpRequest.readLines = function()
    local i = 0
    local ls = {}
    for l in HttpRequest.content:gmatch("(.+)\r?\n?") do
      table.insert(ls,l)
    end
    return function()
      i = i + 1
      return ls[i]
    end
  end
  NativeHttpRequest.close()
  return HttpRequest
end

--- Add HttpConnection to libhttp and return ---
libhttp.HttpConnection = HttpConnection
return libhttp
